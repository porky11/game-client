use game_core::{
    input::{InputMessage, Request},
    world::WorldMessage,
};
use game_player as player;

use communicator::{Sender as _, SimpleCommunicator, StreamCommunicator};
use data_stream::{default_settings::PortableSettings, from_stream};
use ga2::Vector;

use std::{
    net::TcpStream,
    sync::mpsc::{self, Sender},
};

fn communicate(world_sender: Sender<WorldMessage<Vector<f32>>>, mut stream: TcpStream) {
    while let Ok(value) = from_stream::<PortableSettings, _, _>(&mut stream) {
        let _ = world_sender.send(value);
    }
}

fn main() {
    let mut args = std::env::args();
    args.next();

    let ip = args.next();
    let ip = ip.as_deref().unwrap_or("127.0.0.1");

    let port = args.next();
    let port = port.as_deref().unwrap_or("1247");

    let Ok(stream) = TcpStream::connect(format!("{ip}:{port}")) else {
        println!("Server {ip} (port {port}) not available");
        return;
    };

    let (world_sender, world_receiver) = mpsc::channel();

    let mut communicator =
        StreamCommunicator::<PortableSettings, _>::new(stream.try_clone().unwrap());
    let _ = communicator.send(InputMessage::<Vector<f32>>::Request(Request::Reload));

    let graphics_communicator = SimpleCommunicator::new(communicator, world_receiver);
    std::thread::spawn(|| communicate(world_sender, stream));
    player::start(graphics_communicator, 60);
}
